-- SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
--
-- SPDX-License-Identifier: AGPL-3.0-only

-- This file should undo anything in `up.sql`
DROP TABLE users;
DROP TABLE tokens;
DROP TABLE registrations;
