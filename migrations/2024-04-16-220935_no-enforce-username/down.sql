-- SPDX-FileCopyrightText: 2024 Jonah Brüchert <jbb@kaidan.im>
--
-- SPDX-License-Identifier: AGPL-3.0-only

PRAGMA foreign_keys = OFF;

CREATE TABLE registrations_temp (
    username TEXT,
    name TEXT NOT NULL,
    email TEXT PRIMARY KEY NOT NULL,
    alergies TEXT NOT NULL,
    datetime BIGINT NOT NULL,
    payed BOOLEAN NOT NULL,
    helfikon BOOLEAN,
    notes TEXT,
    FOREIGN KEY (username) REFERENCES users (username)
);

INSERT INTO registrations_temp SELECT * FROM registrations;

DROP TABLE registrations;

ALTER TABLE registrations_temp RENAME TO registrations;

PRAGMA foreign_keys = ON;
