// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

extern crate diesel;

use std::io::Cursor;
use std::time::{Duration, SystemTime};

use chrono::{DateTime, Utc};
use rocket::{
    form::Form,
    fs::FileServer,
    get,
    http::{ContentType, Status},
    launch, post,
    request::{FromRequest, Outcome, Request},
    response::Redirect,
    routes,
    serde::{Deserialize, Serialize},
    uri, State,
};

use diesel::result::{DatabaseErrorKind, Error::DatabaseError};

use rocket_dyn_templates::Template;

use spline_oauth::{routes::AuthenticatedUser, OAuthConfig, OAuthSupport};

mod database;
mod schema;
mod templates;

use database::SoFaDb;

fn default_note_description() -> String {
    "Hinweis für die Orga:".to_string()
}

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct EventConfig {
    pub name: String,
    pub description: String,
    #[serde(default = "default_note_description")]
    pub note_field_description: String,
    pub login_description: String,
}

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct OrgaConfig {
    pub members: Vec<String>,
    pub registration_open: bool,
}

#[allow(dead_code)]
struct OrgaMember {
    user: AuthenticatedUser,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for OrgaMember {
    type Error = ();

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let user = AuthenticatedUser::from_request(req).await;
        if let Outcome::Success(user) = user {
            let orga_members = &req.rocket().state::<OrgaConfig>().unwrap().members;
            if orga_members.contains(&user.username) {
                Outcome::Success(OrgaMember { user })
            } else {
                Outcome::Error((Status::Unauthorized, ()))
            }
        } else {
            let (status, _) = user.failed().unwrap();
            Outcome::Error((status, ()))
        }
    }
}

#[get("/")]
async fn index(event_config: &State<EventConfig>) -> Template {
    #[derive(Serialize)]
    #[serde(crate = "rocket::serde")]
    struct Context {
        auth_url: String,
        login_text: String,
    }

    let home = uri!(home).to_string();

    Template::render(
        "index",
        Context {
            auth_url: uri!(spline_oauth::routes::login(home)).to_string(),
            login_text: event_config.login_description.clone(),
        },
    )
}

#[get("/home")]
async fn home(
    user: AuthenticatedUser,
    config: &State<OrgaConfig>,
    event_config: &State<EventConfig>,
) -> Template {
    #[derive(Serialize)]
    #[serde(crate = "rocket::serde")]
    struct Context {
        is_orga_member: bool,
        event_name: String,
        registration_open: bool,
    }

    Template::render(
        "home",
        Context {
            is_orga_member: config.members.contains(&user.username),
            event_name: event_config.name.clone(),
            registration_open: config.registration_open,
        },
    )
}

#[get("/register")]
async fn register(
    user: AuthenticatedUser,
    db: SoFaDb,
    event_config: &State<EventConfig>,
    config: &State<OrgaConfig>,
) -> Template {
    let user = db.get_user(user.username).await.expect("Fetch user data");

    #[derive(Serialize)]
    #[serde(crate = "rocket::serde")]
    struct Context {
        username: String,
        email: String,
        event_description: String,
        registration_open: bool,
        note_field_description: String,
    }

    Template::render(
        "register",
        Context {
            username: user.username,
            email: user.email,
            event_description: event_config.description.clone(),
            registration_open: config.registration_open,
            note_field_description: event_config.note_field_description.clone(),
        },
    )
}

#[get("/thanks?<already_registered>")]
async fn thanks(_user: AuthenticatedUser, already_registered: bool) -> Template {
    #[derive(Serialize)]
    #[serde(crate = "rocket::serde")]
    struct Context {
        already_registered: bool,
    }

    Template::render("thanks", Context { already_registered })
}

pub fn now_in_unix() -> i64 {
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs()
        .try_into()
        .expect("Value doesn't fit into i64 anymore. Time to upgrade!")
}

#[post("/submit-registration", data = "<registration>")]
async fn submit_registration(
    _user: AuthenticatedUser,
    db: SoFaDb,
    config: &State<OrgaConfig>,
    registration: Form<database::RegistrationForm>,
) -> Redirect {
    if !config.registration_open {
        return Redirect::to(uri!(home));
    }

    let database::RegistrationForm {
        username,
        email,
        alergies,
        name,
        helfikon,
        notes,
    } = registration.into_inner();
    let registration = database::Registration {
        username: Some(username),
        email,
        alergies,
        name,
        datetime: now_in_unix(),
        payed: false,
        helfikon: Some(helfikon),
        notes: Some(notes),
    };

    let result = db.save_registration(registration).await;
    match result {
        Ok(_) => Redirect::to(uri!(thanks(false))),
        Err(DatabaseError(DatabaseErrorKind::UniqueViolation, _)) => {
            Redirect::to(uri!(thanks(true)))
        }
        Err(err) => panic!("{1}: {:?}", err, "Failed to save registration"),
    }
}

#[get("/orga")]
async fn orga(_user: OrgaMember, db: SoFaDb) -> Template {
    #[derive(Serialize)]
    #[serde(crate = "rocket::serde")]
    struct Context {
        registrations: Vec<database::Registration>,
        emails: Vec<String>,
        unpaid_emails: Vec<String>,
        paid_emails: Vec<String>,
    }

    let emails = db.get_all_emails().await.expect("Fetching emails");
    let unpaid_emails = db.get_unpaid_emails().await.expect("Fetching emails");
    let paid_emails = db.get_paid_emails().await.expect("Fetching emails");

    let registrations = db
        .get_registrations()
        .await
        .expect("Fetching registrations");

    Template::render(
        "orga",
        Context {
            registrations,
            emails,
            unpaid_emails,
            paid_emails,
        },
    )
}

#[get("/orga/payed?<email>&<payed>")]
async fn set_payed(_user: OrgaMember, db: SoFaDb, email: String, payed: bool) -> Redirect {
    db.set_payed(email, payed).await.expect("Set payed status");

    Redirect::to(uri!(orga))
}

#[get("/orga/delete?<email>")]
async fn delete_registration(_user: OrgaMember, db: SoFaDb, email: String) -> Redirect {
    db.delete(email).await.expect("Delete registration");

    Redirect::to(uri!(orga))
}

#[get("/orga/export")]
async fn export(_user: OrgaMember, db: SoFaDb) -> (ContentType, Vec<u8>) {
    let registrations = db
        .get_registrations()
        .await
        .expect("Getting registrations from the db");

    let mut out = Vec::<u8>::new();

    {
        let mut writer = csv::Writer::from_writer(Cursor::new(&mut out));

        for registration in registrations {
            writer
                .write_record(&[
                    registration.name,
                    registration.username.unwrap_or(String::new()),
                    registration.email,
                    registration.alergies,
                    DateTime::<Utc>::from(
                        SystemTime::UNIX_EPOCH + Duration::from_secs(registration.datetime as u64),
                    )
                    .format("%d %b %Y %H:%M %Z")
                    .to_string(),
                    if registration.helfikon.is_some() && registration.helfikon.unwrap() {
                        "Helfikon".to_string()
                    } else if registration.helfikon.is_some() && !registration.helfikon.unwrap() {
                        "Ersti".to_string()
                    } else {
                        String::new()
                    },
                    registration.notes.unwrap_or_default(),
                    if registration.payed {
                        "bezahlt".to_string()
                    } else {
                        "nicht bezahlt".to_string()
                    },
                ])
                .expect("Failed to write to csv");
        }
    }

    (ContentType::CSV, out)
}

#[get("/privacy")]
async fn privacy() -> Template {
    #[derive(Serialize)]
    #[serde(crate = "rocket::serde")]
    struct Context {}
    Template::render("privacy", Context {})
}

#[launch]
fn rocket() -> _ {
    let rocket = rocket::build();
    let figment = rocket.figment();
    let oauth_config: OAuthConfig = figment
        .extract_inner("oauth")
        .expect("Reading oauth config");
    let orga_config: OrgaConfig = figment.extract_inner("orga").expect("Reading orga config");
    let event_config: EventConfig = figment
        .extract_inner("event")
        .expect("Reading event config");

    rocket
        .attach(OAuthSupport::fairing(oauth_config))
        .manage(orga_config)
        .manage(event_config)
        .mount(
            "/",
            routes![
                index,
                privacy,
                home,
                register,
                thanks,
                submit_registration,
                orga,
                set_payed,
                delete_registration,
                export
            ],
        )
        .attach(templates::TemplateExtensions::fairing())
        .attach(SoFaDb::fairing())
        .mount("/css", FileServer::from("css"))
        .mount("/img", FileServer::from("img"))
}
